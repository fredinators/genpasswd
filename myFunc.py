# -*-coding: utf-8 -*-
"""
# @Time:2021/11/02
# @Author:wubangying
# @Description:
"""
# 将不足3位的网段补足位3位
def myPadding(num1, num2):
    if len(num1) == 1:
        num1 = '00' + num1
    elif len(num1) == 2:
        num1 = '0' + num1
    if len(num2) == 1:
        num2 = '00' + num2
    elif len(num2) == 2:
        num2 = '0' + num2
    return num1, num2

# 映射表
def myMapRule(value, myDict):
    return myDict[value[0]]+myDict[value[1]]+myDict[value[2]]

# 将字符串中的数字和字符分开
def splitNumLetter(myStr):
    nums, letterrs = '', ''
    for i in myStr:
        if i.isdigit():
            nums = nums+i
        else:
            letterrs += i
    return nums, letterrs
